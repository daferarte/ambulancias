const { Router } = require('express');

const { agregarUsuarios, listarUsuarios } = require('../controllers/usuarios.controller');

const rutasrUsuarios = Router();

rutasrUsuarios.get('/v1/', listarUsuarios);
rutasrUsuarios.post('/v1/', agregarUsuarios);

module.exports = rutasrUsuarios;