const { response, request } = require('express');
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient()

const agregarUsuarios = async(req = request, res=response) =>{
    
    const {name, email, password}= req.body;

    const result = await prisma.user.create({
        data:{
            name,
            email,
            password
        }
    }).catch((e)=>{
        return e.message;
    }).finally(async ()=>{
        await prisma.$disconnect()
    })


    res.json({
        result
    })

}

const listarUsuarios = async(req = request, res=response) =>{
    
    

    res.json({
        msg: 'Usuario Listado',
    })
    
}

module.exports = {
    agregarUsuarios,
    listarUsuarios
}